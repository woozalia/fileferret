<?php
/*
  CONFIG:
    * make sure all files found in A are in B
    * delete files from A after ensuring they are the same as B
*/

// ACTION FLAGS

define('KF_DOING_OFFLOAD',TRUE);

define('KF_DO_COPY',		TRUE);	// TRUE = if file A missing from B, copy it
define('KF_DO_DEL',		KF_DOING_OFFLOAD);	// TRUE = after verifying that B=A, delete A
define('KF_DO_DEL_LINKS',	KF_DOING_OFFLOAD);	// TRUE = if A is a link, delete it
define('KF_DO_DEL_IGNORED',	KF_DOING_OFFLOAD);	// TRUE = when blacklisted folder found, delete it and everything under it
define('KF_DO_COMP_FOUND',	TRUE);            // TRUE = compare matching filenames to make sure they are really the same data
define('KF_DO_COPY_EMPTY_FOLDERS',	FALSE);

// DISPLAY FLAGS

  // display all folders as they are processed:
define('KBIN_SHOW_FOLDERS',	KF_STATUS_SHOW);		
  // display all files as they are processed
define('KBIN_SHOW_FILES',	KF_STATUS_FILE | KF_STATUS_PATH);
  // display copying process
define('KBIN_SHOW_NEW_FILES',	KF_STATUS_FILE | KF_STATUS_PATH | KF_STATUS_SHOW);
  // display folders created
define('KBIN_SHOW_NEW_FOLDERS',	KF_STATUS_FILE | KF_STATUS_PATH | KF_STATUS_SHOW | KF_STATUS_NEWLINE);
// only show read process for files larger than this
define('KN_SHOW_READ_MIN_BYTES',	1024*1024);

// OTHER OPTION

// list of masks for files that Really Just Don't Matter, so they can be ignored/deleted
$arFilenameBlacklist = array(	// TODO: rename to arFileNamesIgnore
      '/cache/i'	// skip cache folders (anything whose name includes "cache", case-insensitive)
      //'/^cache$/i'	// skip cache folders (anything named "cache", case-insensitive)
      );


// STATLER-NEXTCLOUD on SamEagle backup

// moving bitcoin data off
#define('KFS_URL_A','/home/woozle/AppData/bitcoin-core');
#define('KFS_URL_B','sftp://root@statler.local/mnt/OcTera1/archive/bitcoin-core');

// verifying/offloading videos
define('KFS_URL_A','/home/woozle/Downloads/Plex');
define('KFS_URL_B','sftp://root@statler.local/mnt/HexTera2/archive/Video/movies');
