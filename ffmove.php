<?php namespace fileferret;
/*
  USAGE: edit ffmove.conf.php to change parameters
  ACTION: ensure that A is a subset of B
    Report any missing files, or mismatches (content, timestamp)
  TODO:
  * when copying files longer than one copy-block, create remote with .part extension and rename when complete
  * option to *not* compare files already found (only compare after copying)
  * significantly improved display of progress (folders should be in tree so they don't wrap around)
  HISTORY:
    2017-12-04 started
    2017-12-21 changing to hardwired arguments for now
    2017-12-23 renamed from ensure_subset.php to ffmove.php, putting arguments in ffmove.conf.php
    2021-07-17 somewhat refactored; some bugs fixed
*/

use ferreteria\classloader\cLoader;
use ferreteria\classloader\cLibraryExec;

// ++ SETTINGS ++ //

define('KF_CLI_MODE',TRUE);
#define('KI_READ_BLOCK_BYTES',1048576);
#define('KFN_OUTPUT','ffmove.out.txt');
#define('KFS_OUTPUT',dirname( __FILE__ ).'/'.KFN_OUTPUT);

class cGlobalsDefault {
    public static function BytesToReadPerBlock() : int { return 1048576; }
    public static function FileName_forLogging() : string { return 'ffmove.<stamp>.out.txt'; }
    public static function FileSpec_forLogging() : string { return __DIR__.'/logs/'.self::FileName_forLogging(); }
}

// -- SETTINGS -- //

// status display flags
define('KF_STATUS_SHOW', 	1);	// show the status
define('KF_STATUS_NEWLINE', 	2);	// do a newline after showing (FALSE = stay on same line)
define('KF_STATUS_FILE',	4);	// add to log file
define('KF_STATUS_PATH',	8);	// include full path (FALSE = just current folder/file name)

include('ffmove.conf.php');

define('KS_FMT_TIMESTAMP','Y/m/d H:i:s');

$fp = dirname( __FILE__ );

#$kfpConfigFerreteria = '../git/ferreteria/config';
#require_once('../git/ferreteria/start.php');
require_once('classloader/ClassLoader.php');

//\fcCodeModule::SetDebugMode(TRUE);
/*
new \fcCodeLibrary('ferreteria',$fsFerreteria);
\fcCodeLibrary::Load_byName('ferreteria');

\fcCodeModule::BasePath($fp.'/');
\fcCodeLibrary::BasePath($fp.'/');
*/

cLoader::SetBasePath(__DIR__);
new cLibraryExec('fileferret','0.php');
cLoader::LoadLibrary('fileferret');

#new \fcCodeLibrary('fileferret','@libs.php');
#\fcCodeLibrary::Load_byName('fileferret');

$oApp = new cCliApp();
$oApp->Go(KFS_URL_A,KFS_URL_B);

class cCliApp {

    public function Go(string $urlA,string $urlB) {
        $fs = $this->GetLogFileSpec();
        echo "LOGGING TO: $fs\n";
	
        $sStartTime = $this->GetTimeStamp();
    
        echo "Ensuring that everything in A also exists at B:\n";
        echo " - A: $urlA\n";
        echo " - B: $urlB\n";
	
        $this->SetFolderA($urlA);
        $this->SetFolderB($urlB);
        
        $oMountA = cFolderAnywhere::MakeObject($urlA);
        $oMountB = cFolderAnywhere::MakeObject($urlB);
        
        $oMountA->Open();
        $oMountB->Open();
        
        $fpA = $oMountA->GetLocalSpec();
        $fpB = $oMountB->GetLocalSpec();

        echo <<<__EOT__
=====================
Filespecs as mounted:
 - A: $fpA
 - B: $fpB
=====

__EOT__;

        $this->CheckTree($fpA);
        $this->CheckTree($fpB);
        
        $oPathA = new cPathBase($fpA);
        $oPathB = new cPathBase($fpB);
        
        $oFolder = new cFolderPair($oPathA,$oPathB);
        $oFolder->Ensure(new cPathCompareStatus($oPathA,0,0));
        
        $oMountA->Shut();
        $oMountB->Shut();
        
        $this->ShowMessage('Done.');
        $this->SummarizeReults();
        $this->SaveDetail($sStartTime);
    }

    protected function CheckTree($url) {
        if (!file_exists($url)) {
            // 2018-01-18 Actually, let's just create it.
            //echo "Folder not found: [$url]\n";
            //die();
            echo 'Creating local folder [$url]...';
            mkdir($url,0777,TRUE);	// create recursively if needed
            echo " done.\n";
        }
    }

    // ++ STATUS VALUES ++ //

    private $fpA,$fpB;	// local filespecs
    protected function SetFolderA($fp) { $this->fpA = $fp; }
    protected function GetFolderA() { return $this->fpA; }
    protected function SetFolderB($fp) { $this->fpB = $fp; }
    protected function GetFolderB() { return $this->fpB; }
    
    // -- STATUS VALUES -- //
    // ++ RESULTS DATA ++ //
    
    private $arDiff=[];
    public function RecordConflict(cFilePair $oPair) { $this->arDiff[] = $oPair; }
    protected function GetConflictArray() : array { return $this->arDiff; }
    protected function GetConflictCount() : int { return count($this->GetConflictArray()); }
    protected function RenderConflictList() : string {
        $ar = $this->GetConflictArray();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $oPair) {
                $out .= " - ".$oPair->GetPathA()."\n";
            }
        }
        return $out;
    }
    
    private $arMiss=[];
    private $nBytes=0;
    public function RecordMissing(cFilePair $oPair) {
        $this->arMiss[] = $oPair;
        $this->nBytes += ($oPair->GetFileA()->GetSize());
    }
    protected function GetMissingArray() : array { return $this->arMiss; }
    protected function GetMissingCount() : int { return count($this->GetMissingArray()); }
    protected function RenderMissingList() : string {
        $ar = $this->GetMissingArray();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $oPair) {
                $oFileA = $oPair->GetFileA();
                $out .= " - ".$oFileA->GetSpec()."\n";
            }
        }
        return $out;
    }

    private $arBList=[];
    public function RecordBlacklisted($fp) { $this->arBList[] = $fp; }
    protected function GetBlacklisted() : array { return $this->arBList; }
    protected function GetBlacklistedCount() : int { return count($this->GetBlacklisted()); }
    protected function RenderBlacklisted() : string {
        $ar = $this->GetBlacklisted();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $fp) {
            $out .= " - $fp\n";
            }
        }
        return $out;
    }
    
    
    // ASSUMES: times are different
    private $arTimeDiff=[];
    public function RecordTimeMismatch(string $fs,int $nTimeA,int $nTimeB,string $sType) {
        $sTimeA = date(KS_FMT_TIMESTAMP,$nTimeA);
        $sTimeB = date(KS_FMT_TIMESTAMP,$nTimeB);
        if ($nTimeA > $nTimeB) {
            $sDetail = "A is later ($sType): $sTimeA > $sTimeB";
        } else {
            $sDetail = "B is later ($sType): $sTimeA < $sTimeB";
        }
        $arDetail = array(
          'fs'	=> $fs,
          'det'	=> $sDetail
          );
        $this->arTimeDiff[] = $arDetail;
    }
    protected function GetTimeDiffs() : array { return $this->arTimeDiff; }
    protected function GetTimeDiffCount() : int { return count($this->GetTimeDiffs()); }
    protected function RenderTimeDiffs() : string {
        $ar = $this->GetTimeDiffs();
        if (count($ar) == 0) {
            $out = " = NONE FOUND =\n";
        } else {
            $out = '';
            foreach ($ar as $arDetail) {
                $fs = $arDetail['fs'];
                $sDet = $arDetail['det'];
                $out .= " - $fs\n -- $sDet\n";
            }
        }
        return $out;
    }
    
    private $nCopies=0;
    public function RecordCopyAttempt() { $this->nCopies++; }
    protected function GetCopyCount() { return $this->nCopies; }

    private $arErrors=[];
    public function RecordCopyFailure(string $fsA,string $fsB,int $nPos)
      { $this->arErrors[] = " - Error at ~$nPos bytes left:\n -- A: $fsA\n -> B: $fsB\n"; }
    protected function GetFailureCount() : int { return count($this->arErrors); }
    protected function GetFailureList() : array { return $this->arErrors; }
    protected function RenderFailures() : string {
        $ar = $this->GetFailureList();
        if (count($ar) == 0) {
            $out = " = NO ERRORS =\n";
        } else {
            $out = '';
            foreach ($ar as $s) {
                $out .= $s;
            }
        }
        return $out;
    }
    
    private $arDel=[];
    public function RecordDeletion(string $fs) { $this->arDel[] = $fs; }
    protected function GetDeletionList() : array { return $this->arDel; }
    protected function GetDeletionCount() : int { return count($this->GetDeletionList()); }
    protected function RenderDeletions() : string {
        $ar = $this->GetDeletionList();
        if (count($ar) == 0) {
            $out = " = NOTHING DELETED =\n";
        } else {
            $out = '';
            foreach ($ar as $s) {
            $out .= $s."\n";
            }
        }
        return $out;
    }
    
    // -- RESULTS DATA -- //
    // ++ OUTPUT: RESULTS ++ //

	// simple for now
    protected function SummarizeReults() : void { echo $this->ShowMessage($this->RenderResultSummary()); }
    protected function RenderResultSummary() : string {
        $out = 'SUMMARY:'
          ."\n - # content conflicts = "		. $this->GetConflictCount()
          ."\n - # timestamp conflicts = "		. $this->GetTimeDiffCount()
          ."\n - # files missing from B = "		. $this->GetMissingCount()
            . " (".number_format($this->nBytes)." bytes)"
          ."\n - # folders blacklisted = "		. $this->GetBlacklistedCount()
          ."\n"
          ;
        if (KF_DO_COPY) {
            $out .= 
            " - # files copied = "              . $this->GetCopyCount()
            .' (failures: '.$this->GetFailureCount().')'
            ."\n"
            ;
        }
        if (KF_DO_DEL) {
            $out .=
              " - # deletions = "          . $this->GetDeletionCount()
              ."\n"
              ;
        }
        return $out;
    }
    protected function SaveDetail($sStartTime) {
	
        $out = 
          'Started: '.$sStartTime."\n"
          .'Finished: '.$this->GetTimeStamp() . "\n"
          .'URL A: '.$this->GetFolderA()."\n"
          .'URL B: '.$this->GetFolderB()."\n"
          .$this->RenderResultSummary()
          ."\n== CONFLICTS ==\n"
          ."\n".$this->RenderConflictList()
          ."\n== TIME DIFFS ==\n"
          ."\n".$this->RenderTimeDiffs()
          ."\n== MISSING ==\n"
          ."\n".$this->RenderMissingList()
          ."\n== BLACKLISTED ==\n"
          ."\n".$this->RenderBlacklisted()
          ;
        if (KF_DO_COPY) {
            $out .= 
              "\n== COPY FAILURE ==\n"
              ."\n".$this->RenderFailures()
              ;
        }
        if (KF_DO_DEL) {
            $out .= 
              "\n== FILES DELETED ==\n"
              ."\n".$this->RenderDeletions()
              ;
        }
        $this->LogWrite($out);
    }
    
    private $fsLog = NULL;
    protected function GetLogFileSpec() : string {
        $fsAct = $this->fsLog;
        if (is_null($fsAct)) {
            $fsTplt = cGlobalsDefault::FileSpec_forLogging();
            $sStamp = date('Y-m-d.his');
            $fsAct = str_replace('<stamp>',$sStamp,$fsTplt);
            $this->fsLog = $fsAct;
        }
        return $fsAct;
    }
    protected function LogWrite($s) {
        $fs = $this->GetLogFileSpec();
        $fh = fopen($fs,'a');
        fwrite($fh,"\n".$s);
        fclose($fh);
    }
    /*----
      HISTORY:
        2021-07-16 stamp format was just 'r', not sure why; changing it to KS_FMT_TIMESTAMP
    */
    protected function GetTimeStamp() { return date(KS_FMT_TIMESTAMP); }
    
    // -- OUTPUT: RESULTS -- //
    // ++ OUTPUT: MESSAGES ++ //
    
    /*----
      ACTION: show a new line of text
      HISTORY:
        2021-07-16 changing protected->public because it's called from outside
    */
    public function ShowMessage($s) {
        $out = NULL;
        if ($this->isInLine) {
            $out .= "\n";
            $this->ResetLine();
        }
        $out .= $s."\n";
        echo $out;
    }

    private $tsLastLine = NULL;	// time of last line echo
    private $tsLastPath = NULL;	// time of last full path
    /*
      ACTION: update the current line of text, but only at intervals
    	This needs some rethinking. Probably need to store the line contents. Naming is an issue too.
      HISTORY:
	2019-04-02 making this PROTECTED so callers have to use more specific updates
    */
    protected function UpdateStatus($s) {
	$tsNow = microtime(TRUE);	// time as floating-point (seconds)
	if ((($tsNow - $this->tsLastLine) > 0.1) || $this->GetAlwaysShowNext()) {	// don't show more than 10 updates/second
	    $this->UpdateStatusAlways($s);
	    $this->tsLastLine = $tsNow;
	}
    }

    private $isInLine = FALSE;
    /*----
      ACTION: display the given message from the start of the line (and make sure the line is clear after that)
      
      HISTORY:
        2021-07-16 changing protected->public, because it's called from outside
        * There was a note saying "USAGE: To explicitly invoke it from outside, call AlwaysShowNext();" but
          I'm not sure that was working... and apparently there was only [S|G]etAlwaysShowNext()?
    */
    public function UpdateStatusAlways($s) {
        echo $s;
        if ($this->isInLine) {
            $this->FinishLine(strlen($s));
        }
        $this->isInLine = TRUE;
    }
    
    /*----
      ACTION: Make sure line is clear to end, and CR to start
      INPUT: $nLen
    */
    private $nLastLen = 0;
    protected function FinishLine($nLen) {
	$nThisLen = $nLen;
	$nLastLen = $this->nLastLen;
	$this->nLastLen = $nThisLen;
	
	$out = '';
	// erase trailing bits of previous status
	if ($nLastLen > $nThisLen) {
	    $out .= str_repeat(' ',$nLastLen - $nThisLen);	// . "($nLastLen - $nThisLen)";
	}
	// go to beginning of line
	$out .= "\r";	// \r = CR, no LF
	echo $out;
    }
    protected function ResetLine() {
	$this->isInLine = FALSE;
	$this->nLastLen = 0;
    }

    
    private $doForceNext = FALSE;
    public function SetAlwaysShowNext() {
	$this->doForceNext = TRUE;
    }
    protected function GetAlwaysShowNext() {
	$out = $this->doForceNext;
	$this->doForceNext = FALSE;
	return $out;
    }
    protected function HandleMessage(cPathSeg $oPath,$sText,$bits) {
	if ($bits & (KF_STATUS_SHOW | KF_STATUS_FILE)) {
	    // if we're showing this status at all...
	    if ($bits & KF_STATUS_PATH) {
		$fs = $oPath->GetPath();
	    } else {
		$fs = $oPath->GetName();
	    }
	    $s = $sText.' '.$fs;
	    
	    if ($bits & KF_STATUS_SHOW) {
		if ($bits & KF_STATUS_NEWLINE) {
		    $this->ShowMessage($s);
		} else {
		    $this->UpdateStatus($s);
		}
	    }
	    if ($bits & KF_STATUS_FILE) {
		$this->LogWrite($s);
	    }
	}
    }
    /*----
      HISTORY:
        2021-07-16 changing from protected->public because it's called from outside
          The status system needs reworking.
    */
    public function AddStatus($s) {
        $nThisLen = strlen($s);
        $this->nLastLen += $nThisLen;
        echo $s;
    }
    
      // specialized public fx
    
    public function UpdateFolder(cPathCompareStatus $oStat) {
	$s = "FOLDER ".$oStat->GetProgressText();
	$this->HandleMessage($oStat->GetPathObject(),$s,KBIN_SHOW_FOLDERS);
    }
    public function UpdateNewFolder(cPathSeg $oPath) {
	$s = "! NEW FOLDER ".$oPath->GetSpec();
	$this->HandleMessage($oPath,$s,KBIN_SHOW_NEW_FOLDERS);
    }
    public function UpdateFile(cPathCompareStatus $oStat) {
	$s = " - FILE ".$oStat->GetProgressText();
	$this->HandleMessage($oStat->GetPathObject(),$s,KBIN_SHOW_FILES);
    }
    public function UpdateFileRead(cPathCompareStatus $oPath, cFileCompareStatus $oFile) {
	$nSize = $oFile->GetByteSize();
	if (KN_SHOW_READ_MIN_BYTES <= $nSize) {
	    $dx = $oFile->GetBytesLeft();
	    $sStat = $oPath->GetProgressText();
	    if ($dx > 0) {
		$s = " - FILE $sStat: $dx to read";
	    } else {
		$s = " - FILE $sStat: $nSize bytes ok";
	    }
	    $this->UpdateStatus($s);
	} else {
	    // clear the always-display flag here since it won't get cleared by UpdateStatus()
	    $this->GetAlwaysShowNext();
	}
    }
    public function UpdateCopyStatus(cPathCompareStatus $oPath,$nBytesLeft) {
	$sStat = $oPath->GetProgressText();
	$s = "FILE $sStat: $nBytesLeft bytes to copy";
	$this->UpdateStatus($s);
    }
    public function UpdateCopyingError(cPathCompareStatus $oPath,$nBytesLeft) {
	$sStat = $oPath->GetProgressText();
	$url = $oPath->GetPathObject()->GetSpec();
	$s = "FILE ($sStat) $url";
	$this->ShowMessage($s);
	$s = " ** ERROR copying ($nBytesLeft to go)";
	$this->ShowMessage($s);
    }

    public function AddFolderStatus($s) {
      $this->AddStatus($s);	// a bit of a kluge
    }
    public function AddFileStatus($s) {
      $this->AddStatus($s);	// a bit of a kluge
    }
    
    // TODO: for now this is just an alias for UpdateStatusAlways(), but some rethinking is in order
    /* 2019-04-05 disabling for now while reworking the status system
    public function ShowStatus($s) {
	$this->UpdateStatusAlways($s);
    }
    /*----
      HISTORY:
        2019-04-05 "disabling for now while reworking the status system", along with ShowStatus()
        2021-07-16 re-enabled because it's being called and I guess I never finished reworking?
    */
    public function FinishStatus() {
        echo "\n";
        $this->ResetLine();
    }
    
    // -- OUTPUT -- //
}
