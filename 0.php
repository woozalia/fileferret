<?php namespace fileferret;

#$fp = dirname( __FILE__ );
#fcCodeModule::BasePath($fp.'/');
#fcCodeLibrary::BasePath($fp.'/');

$oLib = $this;

$om = $oLib->MakeModule('class/file.php');
  $om->AddClass(cFile::class);
  
$om = $oLib->MakeModule('class/filePair.php');
  $om->AddClass(cFilePair::class);
  
$om = $oLib->MakeModule('class/folderAnywhere.php');
  $om->AddClass(cFolderAnywhere::class);
  
$om = $oLib->MakeModule('class/folder.php');
  $om->AddClass(cFolder::class);
  
$om = $oLib->MakeModule('class/folderPair.php');
  $om->AddClass(cFolderPair::class);
  
$om = $oLib->MakeModule('class/fsnode.php');
  $om->AddClass(cFSNode::class);
  
$om = $oLib->MakeModule('class/nodePair.php');
  $om->AddClass(cFSNodePair::class);
  
$om = $oLib->MakeModule('class/pathSeg.php');
  $om->AddClass(cPathSeg::class);
  $om->AddClass(cPathBase::class);

$om = $oLib->MakeModule('class/volume.php');
  $om->AddClass(ctVolumes::class);

