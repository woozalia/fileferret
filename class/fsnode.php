<?php namespace fileferret;

/*::::
  HISTORY:
    2018-01-20 created as a common parent for cFile and cFolder
*/
abstract class cFSNode {

    // ++ SETUP ++ //

    public function __construct($fs) { $this->SetSpec($fs); }
    
    private $fs;
    protected function SetSpec($fs) { $this->fs = $fs; }
    public function GetSpec() { return $this->fs; }

    // -- SETUP -- //
    // ++ CALCULATIONS ++ //

    public function GetExists() {
        $uri = $this->GetSpec();
        return file_exists($uri);
    }
    
    private $isLink = NULL;
    public function GetIsLink() {
        if (is_null($this->isLink)) {
            $this->isLink = is_link($this->GetSpec());
        }
        return $this->isLink;
    }
    private $bfPerms = NULL;
    protected function GetPerms() {
        if (is_null($this->bfPerms)) {
              $this->bfPerms = fileperms($this->GetSpec());
        }
        return $this->bfPerms;
    }
    public function GetTimestamp() { return filemtime($this->GetSpec()); }
    public function SetTimestamp(int $dt) { touch($this->GetSpec(),$dt); }
    
    // -- CALCULATIONS -- //
    // ++ ACTIONS ++ //
    
    abstract public function Delete();
    public function DeleteVisibly() {
        global $oApp;
        
        $ok = $this->Delete();
        $fs = $this->GetSpec();
        if ($ok) {
            $oApp->UpdateStatusAlways( "DELETED [$fs]" );
        } else {
            $oApp->ShowMessage( "Could not delete [$fs]." );	// just leave it there, no need to die()
        }
        $oApp->RecordDeletion($fs);
        return $ok;
    }

    // -- ACTIONS -- //

}
