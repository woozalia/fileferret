<?php namespace fileferret;
/*
  HISTORY:
    2017-12-21 extracting general-purpose classes from ensure_subset.php
*/

class cFileCompareStatus {
    private $n,$dx;	// total size, bytes of progress

    public function __construct($n,$dx) {
        $this->n = $n;
        $this->dx = $dx;
    }
    public function GetBytesLeft() { return $this->dx; }
    public function GetByteSize() { return $this->n; }
}

class cFilePair extends cFSNodePair {

    // ++ SETUP ++ //

    public function __construct($urlA,$urlB,$nCount,$nIndex) {
        parent::__construct($urlA,$urlB);
        $this->SetIndices($nCount,$nIndex);
    }
    
    private $nCount,$nIndex;
    protected function SetIndices($nCount,$nIndex) {
	$this->nCount = $nCount;
	$this->nIndex = $nIndex;
    }
    protected function GetIndexString() {
	return $this->nIndex.'/'.$this->nCount;
    }
    protected function IsLastOne() {
	return ($this->nIndex == $this->nCount);
    }
    
    // -- SETUP -- //
    // ++ OBJECTS ++ //
    
    private $oFileA=NULL;
    public function GetFileA() {
	if (is_null($this->oFileA)) {
	    $this->oFileA = $this->GetPathA()->GetFileNode();
	}
	return $this->oFileA;
    }
    private $oFileB=NULL;
    public function GetFileB() {
	if (is_null($this->oFileB)) {
	    $this->oFileB = $this->GetPathB()->GetFileNode();
	}
	return $this->oFileB;
    }
    
    // -- OBJECTS -- //
    // ++ ACTION ++ //
    
    /*----
      ACTION: 
        1. Check that there is a file at $urlB
        2. Make sure it is the same as the file at $urlA
        3. If either condition not met, inform the user.
      FUTURE: Maybe this should be called EnsureB()
    */
    public function Ensure(cPathCompareStatus $oStat) {
        global $oApp;
        
        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
    //	if ($ofB->Exists() && !is_link($urlB)) {	// for now, not dealing with links
        $urlA = $ofA->GetSpec();
            
        // first, make sure neither is a special file
        $isFileA = $ofA->GetIsCopyable();
        $isFileB = $ofB->GetIsCopyable();
        if ($isFileA) {
            $canCopy = FALSE;
            if ($isFileB) {
                // same-name file exists and is actual file, so now let's see if they're the same
                
                // obvious check: are they the same size?
                $nSizeA = $ofA->GetSize();
                $nSizeB = $ofB->GetSize();
                if ($nSizeA == $nSizeB) {
                    if (KF_DO_COMP_FOUND) {
                        $this->ShowCompare($oStat,$nSizeA);
                    }
                } elseif ($nSizeB == 0) {
                    // if target is empty, go ahead and assume it's ok to overwrite
                    $canCopy = TRUE;
                } else {
                    $oApp->ShowMessage("FILE $urlA DIFFERENT SIZE - A=$nSizeA , B=$nSizeB");
                    $oApp->RecordConflict($this);
                }
            } else {
                $canCopy = TRUE;
            }
            if ($canCopy) {
                // NOTE: This can also happen if A is a file and B is a link or pipe
                //$oApp->ShowMessage("NO FILE: $urlB");
                $oApp->RecordMissing($this);
                if (KF_DO_COPY) {
                    if ($ofA->IsPipe()) {
                        $oApp->ShowMessage("FILE $urlA IS A PIPE; skipping.");
                    } else {
                        $ok = $this->CopyAToB($oStat);	// copy to B, verify
                    // TODO: original note indicated that this might delete from A, but actual fx() does not afaict.
                    // TODO: should we be doing something with $ok?
                    }
                }
            }
        } else {
            if ($isFileB) {
                $sDescr = 'local file is not';
            } else {
                $sDescr = 'neither local nor remote is';
            }
            $oApp->ShowMessage("FILE $urlA: ".$sDescr.' a copyable file');
            if (KF_DO_DEL_LINKS && $ofA->GetIsLink()) {
                $ofA->DeleteVisibly();
            }
        }
    }
    /*----
      ACTION: compare the contents of the two files, byte for byte
      ASSUMES: files are same size
    */
    protected function ShowCompare(cPathCompareStatus $oPathStat, $nSize) {
        global $oApp;
        
        //$urlA = $this->GetURLA();
        //$urlB = $this->GetURLB();
        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $urlA = $ofA->GetSpec();
        $urlB = $ofB->GetSpec();

        $nBytesLeft = $nSize;
        $nBlockBytes = cGlobalsDefault::BytesToReadPerBlock();
        $ok = TRUE;
        $fpA = $ofA->OpenForRead();
        $fpB = $ofB->OpenForRead();

        while($ok && ($nBytesLeft > 0)) {
            $sFileA = fread($fpA,$nBlockBytes);
            $sFileB = fread($fpB,$nBlockBytes);
            if ($sFileA != $sFileB) {
                $oApp->ShowMessage("FILE [$urlA] CONTENTS ARE DIFFERENT");
                $oApp->RecordConflict($this);
                $ok = FALSE;
            } elseif($sFileA === FALSE) {
                $oApp->ShowMessage("FILE [$urlA] COULD NOT BE READ");
                $ok = FALSE;
            } else {
                $nBytesLeft -= strlen($sFileA);
                $sIdx = $this->GetIndexString();
                $oFileStat = new cFileCompareStatus($nSize,$nBytesLeft);
                if ($nBytesLeft > 0) {
                    $oApp->UpdateFileRead($oPathStat,$oFileStat);
                } else {
                    if (($nBytesLeft > $nBlockBytes) || $this->IsLastOne()) {
                        $oApp->SetAlwaysShowNext();
                    }
                    $sMsg = "$sIdx : $nSize bytes ok";
                    $oApp->UpdateFileRead($oPathStat,$oFileStat);
                }
            }
        }
        
        // close files
        $ofA->Shut();
        $ofB->Shut();
        
        if ($ok) {	// if file contents are equal...
            // 2017-12-24 apparently filectime cannot be modified programmatically, so no point in checking it
        
            // if file content matches, check timestamps
    //	    $nTimeA = filectime($urlA);
    //	    $nTimeB = filectime($urlB);
    //	    if ($nTimeA == $nTimeB) {
            $nTimeA = filemtime($urlA);
            $nTimeB = filemtime($urlB);
            if ($nTimeA != $nTimeB) {
                $oApp->RecordTimeMismatch($urlA,$nTimeA,$nTimeB,'mod');
                $ok = FALSE;
                echo "TIME MISMATCH\n";
            }
    //	    } else {
    //		$oApp->RecordTimeMismatch($urlA,$nTimeA,$nTimeB,'cre');
    //		$ok = FALSE;
    //	    }
        }
        //echo "VERIFIED ok=[$ok]: $urlA\n";
        if ($ok && KF_DO_DEL) {
            // A and B are identical, and we're deleting identical files from A, so:
            $this->DeleteA();
        }
        return $ok;
    }
    protected function CopyAToB(cPathCompareStatus $oPathStat) {
        global $oApp;

        $ofA = $this->GetFileA();
        $ofB = $this->GetFileB();
        $nSize = $ofA->GetSize();

        $urlA = $ofA->GetSpec();
        $urlB = $ofB->GetSpec();
        
        $nBytesLeft = $nSize;
        $ok = TRUE;
        //$fpA = fopen($urlA,'r');
        //$fpB = fopen($urlB,'w');	// B may exist as a pipe or link; this will overwrite
        
        $fpA = $ofA->OpenForRead();
        $fpB = $ofB->OpenForWrite();
        $oApp->RecordCopyAttempt();
        $nBlockBytes = cGlobalsDefault::BytesToReadPerBlock();
        while($ok && ($nBytesLeft > 0)) {
            $sFileA = fread($fpA,$nBlockBytes);
            $nCopied = fwrite($fpB,$sFileA);
            if ($nCopied === FALSE) {
                $ok = FALSE;
                //$sMsg = "FILE $urlA - ERROR copying ($nBytesLeft to go)";
                $oApp->UpdateCopyingError($oPathStat,$nBytesLeft);
                //$oApp->UpdateStatusAlways($sMsg);
                $oApp->RecordCopyFailure($urlA,$urlB);
            } else {
                $nBytesLeft -= strlen($sFileA);
                $oApp->UpdateCopyStatus($oPathStat,$nBytesLeft);
            }
        }
        
        // if there were no errors copying:
        if ($ok) {
            // set the timestamp on B:
            $nTime = filemtime($urlA);
            touch($urlB,$nTime);
            
            // verify same content, to make sure:
            $this->ShowCompare($oPathStat,$nSize);
        }
        return $ok;
    }
    protected function DeleteA() {
	global $oApp;

	$ok = $this->GetFileA()->DeleteVisibly();
	return $ok;
    }
}
