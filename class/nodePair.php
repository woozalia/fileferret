<?php namespace fileferret;
/*
  HISTORY:
    2017-12-21 extracting general-purpose classes from ensure_subset.php
    2019-04-05 added cPathCompareStatus
*/
class cPathCompareStatus {
    private $n,$dx,$oPath;

    public function __construct(cPathSeg $oPath, $n,$dx) {
	$this->oPath = $oPath;
	$this->n = $n;
	$this->dx = $dx;
    }
    public function GetProgressText() {
	return $this->dx.'/'.$this->n;
    }
    public function GetPathObject() {
	return $this->oPath;
    }
}

class cFSNodePair {

    // ++ SETUP ++ //

    public function __construct(cPathSeg $oPathA,cPathSeg $oPathB) { $this->SetPaths($oPathA,$oPathB); }
    private $oPathA,$oPathB;
    protected function SetPaths(cPathSeg $oPathA,cPathSeg $oPathB) {
        $this->oPathA = $oPathA;
        $this->oPathB = $oPathB;
    }
    // PUBLIC so results can be displayed
    public function GetPathA() : cPathSeg { return $this->oPathA; }
    protected function GetPathB() : cPathSeg { return $this->oPathB; }

    // -- SETUP -- //
}
