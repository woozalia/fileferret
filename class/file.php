<?php namespace fileferret;
/*
  HISTORY:
    2017-12-21 created to make it easier to sum up file sizes without trying to stat un-stat-able files
      need to encapsulate detection of whether something is a copyable file, at least
    2018-01-19 added GetIsCopyable()
*/
class cFile extends cFSNode {

    // ++ STATUS ++ //

    private $fh=NULL;	// file handle
    protected function SetNative($fh) {
	$this->fh = $fh;
    }
    protected function GetNative() {
	return $this->fh;
    }
    protected function IsOpen() {
	return (!is_null($this->GetNative()));
    }
    
    // -- STATUS -- //
    // ++ CALCULATIONS ++ //
    
    private $bExists = NULL;
    public function GetExists() {
	if (is_null($this->bExists)) {
	    $this->bExists = file_exists($this->GetSpec());
	}
	return $this->bExists;
    }
    
    private $isFile = NULL;
    public function GetIsFile() {
	if (is_null($this->isFile)) {
	    $this->isFile = is_file($this->GetSpec());
	}
	return $this->isFile;
    }
    private $canCopy = NULL;
    public function GetIsCopyable() {
	if (is_null($this->canCopy)) {
	    $this->canCopy = is_readable($this->GetSpec());	// best guess: if it can be read, it can be copied
	}
	return $this->canCopy;
    }
    public function IsPipe() {
	return (($this->GetPerms() & 010000) != 0);
    }
    private $nSize = NULL;
    public function GetSize() {
	if ($this->GetIsFile()) {
	    $this->nSize = filesize($this->GetSpec());
	}
	return $this->nSize;
    }

    // -- CALCULATIONS -- //
    // ++ OPERATIONS ++ //
    
    public function OpenForRead() {
	return fopen($this->GetSpec(),'r');
    }
    // ACTION: opens a file for writing; creates if not found; creates parent folder if not found
    public function OpenForWrite() {
	$fs = $this->GetSpec();
	$fp = dirname($fs);
	if (!file_exists($fp)) {
	    $ok = mkdir($fp,0777,TRUE);
	    if (!$ok) {
		die("COULD NOT CREATE [$fp].\n");
	    }
	}
	return fopen($fs,'w');
    }
    public function Shut() {
	if ($this->IsOpen()) {
	    fclose($this->GetNative());
	    $this->SetNative(NULL);
	}
    }

    // -- OPERATIONS -- //
    // ++ ACTIONS ++ //
    
    // CEMENT
    public function Delete() {
	$fs = $this->GetSpec();
	$ok = unlink($fs);
	return $ok;
    }

    // -- ACTIONS -- //

}