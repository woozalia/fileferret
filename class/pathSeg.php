<?php namespace fileferret;
/*
  PURPOSE: a piece of a file path
  HISTORY:
    2019-03-31 started, so we can show paths as trees
*/

class cPathStatus {
    public $nSegs;
    public $fPath;	// relative path
    public $fSpec;	// absolute path
    
    public function __construct() {
	$this->nSegs = 0;
	$this->fPath = '';
	$this->fSpec = '';
    }
    public function GetIndentString() {
	if ($this->nSegs == 0) {
	    return '';
	} else {
	    return str_repeat('  ',$this->nSegs);
	}
    }
}

class cPathSeg {

    protected function __construct($sName, cPathSeg $oBase) {
        $this->SetName($sName);
        $this->SetBase($oBase);
    }

    private $fn;
    protected function SetName($fn) { $this->fn = $fn; }
    // RETURNS entire filespec, including base
    public function GetSpec() { return $this->GetPathStatus()->fSpec; }
    /*----
      RETURNS relative filespec from base
      PUBLIC so display routines can use it
    */
    public function GetPath() {
	return $this->GetPathStatus()->fPath;
    }
    // PUBLIC so we can display just the latest addition to the path
    public function GetName() {
	return $this->fn;
    }
    
    private $oBase;
    protected function SetBase(cPathSeg $oBase=NULL) {
	$this->oBase = $oBase;
    }
    protected function GetBasePath() {
	if (is_null($this->oBase)) {
	    return '';
	} else {
	    return $this->oBase->GetPath();
	}
    }
    // API
    public function GetPathStatus() : cPathStatus {
	$oStat = $this->oBase->GetPathStatus();
	$oStat->nSegs++;
	$sName = $this->GetName();
	$oStat->fPath .= '/'.$sName;
	$oStat->fSpec .= '/'.$sName;
	return $oStat;
    }
    public function GetFileNode() {
	$fs = $this->GetSpec();
	return new cFile($fs);
    }
    public function GetFolderNode() {
	$fs = $this->GetSpec();
	return new cFolder($fs);
    }
    
    public function AddSegment($fn) : cPathSeg {
	return new cPathSeg($fn,$this);
    }
    
    /*
    public function DisplayIndented($sPfx,$bNewLine) {
	global $oApp;
    
	$oStat = $this->GetPathStatus();
	$fp = $this->GetPath();
	$sShow = $oStat->GetIndentString() . $sPfx . ' '. $fp;
	$oApp->SetPath($fp);
	$oApp->UpdateStatus($sShow);
	if ($bNewLine) {
	    $oApp->FinishStatus();
	}
    } */
}

class cPathBase extends cPathSeg {
    public function __construct($fp) {
	$this->SetSpec($fp);
    }
    
    private $fpBase;
    protected function SetSpec($fp) {
	$this->fpBase = $fp;
    }
    public function GetPathStatus() : cPathStatus {
	$oStat = new cPathStatus();
	$oStat->fSpec = $this->fpBase;
	$oStat->fPath = '';
	return $oStat;
    }
}
