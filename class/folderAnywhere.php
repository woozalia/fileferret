<?php namespace fileferret;

/*
  HISTORY:
    2017-12-08 (or thereabouts): created to manage mounting of remote file systems for ensure_subset.php
      (which will probably get renamed at some point)
    2021-07-15 tidying and updating
*/

define('KFP_MOUNT_ROOT','/mnt/FileFerret');

// PURPOSE: translates URLs into file paths, mounting remote folders as needed
abstract class cFolderAnywhere {

    // ++ SETUP ++ //

    /*----
      PURPOSE: Check to see if the location has already been mounted. Mount it if not.
      ACTION:
        * if URL includes a protocol, parse it: proto://[user@]host/path
        * invoke the appropriate class to handle the URL
    */
    static public function MakeObject($url) {
        // see if there is a ':' before the first '/'
        $idxColon = strpos($url,':');
        $idxSlash = strpos($url,'/');
        //echo "COLON=[$idxColon] SLASH=[$idxSlash]\n";
        if ($idxColon < $idxSlash) {
            // ...then the URL starts with a protocol.
            $sProto = substr($url,0,$idxColon); // get the protocol string
            $sRem = substr($url,$idxColon+1);   // "://" etc.
            echo "SREM=[$sRem]\n";
            $idxSlash = strpos($sRem,'//');
            $sEtc = substr($sRem,$idxSlash+2);  // everything after "://"
            echo "SETC=[$sEtc]\n";
            $idxSlash = strpos($sEtc,'/');
            $sConn = substr($sEtc,0,$idxSlash); // connection ([user@]host)
            $sPath = substr($sEtc,$idxSlash);	  // don't include first '/'
            echo "SCONN=[$sConn]\n";
            
            $idxAt = strpos($sConn,'@');            // check for optional user@
            if ($idxAt !== FALSE) {
                $sUser = substr($sConn,0,$idxAt);
                $sHost = substr($sConn,$idxAt+1);
            } else {
                $sUser = NULL;  // for diagnostic
                $sHost = $sConn;
            }
            
            #echo "URL=[$url]\n";
            #echo "PROTO=[$sProto] USER=[$sUser] HOST=[$sHost] PATH=[$sPath]\n"; die();
            $oFolder = self::MakeMount($sProto,$sUser,$sHost,$sPath);
        } else {
            $sPath = $url;
            //echo "PATH=[$sPath]\n";
            $oFolder = new cFolderLocal($sPath);
        }
        return $oFolder;
    }
    static private $arMounts = array();
    static protected function MakeMount(string $sProto,?string $sUser,string $sHost,string $sPath) {
        switch ($sProto) {
          case 'sftp':
            $sClass = cFolderSFTP::class;
            break;
          case 'dav':
            $sClass = cFolderDAV::class;
            break;
          default:
            echo "ERROR: protocol [$sProto] is not currently supported.\n";
            die();
        }
        #$sKey = "$sProto/$sUser/$sHost";
        
        // this is wrong, and results in the second path overriding the first
        //if (array_key_exists($sKey,self::$arMounts)) {
        //    $oMount = self::$arMounts[$sKey];
        //} else {
            $oMount = new $sClass($sUser,$sHost,$sPath);
            //self::$arMounts[$sKey] = $oMount;
        //}
        return $oMount;
    }
    
    // -- SETUP -- //
    // ++ VALUE CALCULATIONS ++ //

    abstract public function GetLocalSpec() : string ;
    
    // -- VALUE CALCULATIONS -- //
    // ++ LIFECYCLE ++ //
    
    abstract public function Open();
    abstract public function Shut();

    // -- LIFECYCLE -- //
}

class cFolderLocal extends cFolderAnywhere {
    public function __construct($url) { $this->SetLocalSpec($url); }
    
    // ++ LIFECYCLE ++ //
    
    public function Open()	{}	// no action needed
    public function Shut()	{}	// no action needed

    // -- LIFECYCLE -- //
    // ++ VALUE ACCESS ++ //
    
    private $fp;
    protected function SetLocalSpec(string $fp) { $this->fp = $fp; }
    // CEMENT
    public function GetLocalSpec() : string { return $this->fp; }
}
// PURPOSE: Folder on a remote server which gets mounted somewhere locally
abstract class cFolderRemote extends cFolderAnywhere {

    // ++ SETUP ++ //

    public function __construct(?string $sUser,string $sHost,string $sPath) {
        $this->SetUser($sUser);
        $this->SetHost($sHost);
        $this->SetPath($sPath);
    }

    // -- SETUP -- //
    // ++ VALUE ACCESS ++ //

    private $sUser=NULL;
    protected function SetUser(?string $s) { $this->sUser = $s; }
    protected function GetUser() : ?string { return $this->sUser; }
    
    private $sHost=NULL;
    protected function SetHost(string $s) { $this->sHost = $s; }
    protected function GetHost() : string { return $this->sHost; }
    
    private $sPath=NULL;
    protected function SetPath(string $s) { $this->sPath = $s; }
    protected function GetPath() : string { return $this->sPath; }
    
    // for making unique mountpoints
    private $sStamp = NULL;
    protected function GetStamp() : string {
        if (is_null($this->sStamp)) {
            $this->sStamp = date('Y-m-d.His');
        }
        return $this->sStamp;
    }
    
    // -- VALUE ACCESS -- //
    // ++ VALUE CALCULATIONS ++ //
    
    protected function GetMountFileSpec() : string {
        $sHost = $this->GetHost();
        #$sUser = $this->GetUser();
        #$sPath = $this->GetPath();
        $sStamp = $this->GetStamp();
        $fpMounts = KFP_MOUNT_ROOT;
        $fsMount = "$fpMounts/$sHost-$sStamp";
        return $fsMount;
    }
    public function GetLocalSpec() : string { return $this->GetMountFileSpec(); } // ALIAS
    /*----
      RETURNS: spec for connection
        * [user@]host/path
        * no protocol
    */
    protected function GetConnSpec() : string {
        $sHost = $this->GetHost();
        $sUser = $this->GetUser();
        
        return (is_null($sUser)?'':"$sUser@") . $sHost;
    }
    protected function GetPath_esc() : string {
        return addslashes($this->GetPath());
    }
    abstract protected function GetMountCommand() : string;
    protected function GetUnmountCommand() {
        $fpLocal = $this->GetMountFileSpec();
        $fpqLocal = addslashes($fpLocal); // escape the spec string for CLI usage
        $sCmd = 'umount '.$fpqLocal;
        return $sCmd;
    }
    
    // -- VALUE CALCULATIONS -- //
    // ++ OPERATIONS ++ //

    protected function CheckMountpoint() : bool {
        $fpLocal = $this->GetMountFileSpec();	// where the remote will appear locally
        if (file_exists($fpLocal)) {
            // make sure nothing is mounted (2021-07-15 I *think* this is to prevent collision with incomplete processes?)
            echo " - Error: Duplicate mount point [$fpLocal] found; making sure it is unmounted...";
            $sCmd = $this->GetUnmountCommand();
            exec($sCmd,$arOut,$nStatus);
            echo "done (exit code $nStatus)\n";
            $ok = FALSE;
        } else {
            echo " - Creating folder [$fpLocal]...";
            $ok = mkdir($fpLocal,0777,TRUE);	// create the root mount folder (recursively if needed)
            $sStatus = $ok?'ok':'ERROR';
            echo $sStatus."\n";
        }
        return $ok;
    }
    
    public function Open() {
        $sHost = $this->GetHost();
        $fpRemote = $this->GetPath();
        
        echo "Creating mountpoint:\n";
        
        if ($this->CheckMountpoint()) {
            $sCmd = $this->GetMountCommand();
            echo " - Mounting $fpRemote (on $sHost):"
              ."\n -- command: [$sCmd]"
              ."\n -- "
              ;
            exec($sCmd,$arOut,$nStatus);
            if ($nStatus == 0) {
                echo "ok.\n";
            } else {
                echo "ERROR $nStatus; unmounting...";
                $sCmd = $this->GetUnmountCommand();
                exec($sCmd,$arOut,$nStatus);
                echo " unmount complete (exit code $nStatus)\n";
                die();
            }
        } else {
            die();
        }
    }
    public function Shut() {
        $fpLocal = $this->GetMountFileSpec();
        echo " - Closing (unmounting) [$fpLocal]...";
        $sCmd = $this->GetUnmountCommand();
        exec($sCmd,$arOut,$nStatus);
        echo "done (exit code $nStatus)\n";
    }
    
    // -- OPERATIONS -- //
}
/*::::
  NOTE:
    sshfs [user@]host:[dir] mountpoint [options]
*/
class cFolderSFTP extends cFolderRemote {
    protected function GetMountCommand() : string {
        $sConn = $this->GetConnSpec();
        $fpqRemote = $this->GetPath_esc();
        $fpMount = $this->GetMountFileSpec();
        $fpqMount = addslashes($fpMount);
        echo "SCONN=[$sConn]\n";
        return "sshfs '$sConn:$fpqRemote' '$fpqMount'";
    }
}
/*
  NOTE:
    2021-07-15 not sure this was ever tested
*/
class cFolderDAV extends cFolderRemote {
	// mount -t davfs office.sageandswift.com/owncloud/remote.php/webdav /mnt/test
    protected function GetMountCommand() : string {
        $sConn = $this->GetConnSpec();
        $fpqPath = $this->GetPath_esc();
        $fpqMount = '"'.addslashes($this->GetMountFileSpec()).'"';
        return "mount -t davfs $sConn$fpqPath $fpqMount";
    }
}
