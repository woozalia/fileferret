<?php namespace fileferret;
/*
  HISTORY:
    2018-01-20 created for tree-delete functionality, but probably more stuff should be moved in here
*/

class cFolder extends cFSNode {

    // ++ CALCULATIONS ++ //

    static public function GetDirectoryStringArraysFor($url) {
        $ar = self::GetDirectoryStringArrayFor($url);
        $arFiles = NULL;
        $arFldrs = NULL;
        foreach ($ar as $fn => $fs) {
            if (is_dir($fs)) {
            // save in an array so we can sort before loading
            $arFldrs[$fn] = $fs;
            } else {
            $arFiles[$fn] = $fs;
            }
        }
        $arOut = array('fi'=>$arFiles,'fo'=>$arFldrs);
        return $arOut;
    }
    static protected function GetDirectoryStringArrayFor($url) {
        $poDir = dir($url);
        if (is_object($poDir)) {
            $ar = array();
            
            //$doDebug = (strpos($url,'Copy of harena') !== FALSE);
            $doDebug = FALSE;
            if ($doDebug) {
            echo "DIRECTORY OF [$url]:\n";
            }
            while (FALSE !== ($fn = $poDir->read())) {
            if (($fn!='.') && ($fn!='..')) {
                $fs = $url.'/'.$fn;
                $ar[$fn] = $fs;
                if ($doDebug) {
                echo " - FN=[$fn]\n";
                }
            }
            }
        } else {
            throw new \exception("Some kind of error trying to read directory of [$url].\n");
        }
        return $ar;
    }
    protected function GetDirectoryStringArray() { return self::GetDirectoryStringArrayFor($this->GetSpec()); }
    protected function GetDirectoryObjectArrays() {
        $sar = $this->GetDirectoryStringArray();
        $oarFi = array();
        $oarFo = array();
        
        foreach ($sar as $fn => $fs) {
            if (is_dir($fs)) {
                echo "CREATING FOLDER-OBJECT [$fs]\n";
                $of = new cFolder($fs);
                $oarFo[$fn] = $of;
            } else {
                echo "CREATING FILE-OBJECT [$fs]\n";
                $of = new cFile($fs);
                $oarFi[$fn] = $of;
            }
        }
        $arOut = array('fi'=>$oarFi,'fo'=>$oarFo);
        return $arOut;
    }

    // -- CALCULATIONS -- //
    // ++ ACTIONS ++ //

    public function Create() { mkdir($this->GetSpec()); }

    // CEMENT
    public function Delete() {
        $fs = $this->GetSpec();
        if ($this->GetIsLink()) {
            $ok = @unlink($fs); // delete the file that is the link, not the folder linked to
        } else {
            $ok = @rmdir($fs);  // delete the (presumably empty) folder
        }
        return $ok;
    }
    // ACTION: Delete this folder and everything under it, recursively
    public function DeleteTree() {
        global $oApp;

        $oars = $this->GetDirectoryObjectArrays();
        $oarFi = $oars['fi'];
        $oarFo = $oars['fo'];

        //echo 'DIR ARRAY:'.\fcArray::Render($oars);

        // STEP 1: empty out the subfolders
        foreach ($oarFo as $fn => $of) {
            $of->DeleteTree();
            /* this is redundant
            $ok = $of->Delete();
            if ($ok) {
            $oApp->ShowMessage(" + DELETED [$fs]");
            } else {
            $fs = $of->GetSpec();
            $oApp->ShowMessage("ERROR deleting folder [$fs]");
            } */
        }

        // STEP 2: empty out this folder
        foreach ($oarFi as $fn => $of) {
            $ok = $of->Delete();
            $fs = $of->GetSpec();
            $oApp->ShowMessage(" = DELETED [$fs]");
            if (!$ok) {
            $fs = $of->GetSpec();
            $oApp->ShowMessage("ERROR deleting file [$fs]");
            }
        }

        // STEP 3: delete this folder
        $ok = $this->Delete();
        $fs = $this->GetSpec();
        if ($ok) {
            $oApp->ShowMessage(" < DELETED [$fs]");
        } else {
            $fs = $of->GetSpec();
            $oApp->ShowMessage("ERROR deleting folder [$fs]");
        }
    }

    // -- ACTIONS -- //
}
