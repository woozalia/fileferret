<?php namespace fileferret;
/*
  HISTORY:
    2017-12-21 extracting general-purpose classes from ensure_subset.php
*/
class cFolderPair extends cFSNodePair {

    // ++ OBJECTS ++ //
    
    private $oFA=NULL;
    public function GetFolderA() {
        if (is_null($this->oFA)) {
            $this->oFA = new cFolder($this->GetPathA());
        }
        return $this->oFA;
    }
    
    // -- OBJECTS -- //
    // ++ ACTIONS ++ //
    
    // FUTURE: Maybe this should be called EnsureB()
    public function Ensure(cPathCompareStatus $oStat) {
        global $oApp;
        
        $oPathA = $this->GetPathA();
        $oPathB = $this->GetPathB();
        
        $uriA = $oPathA->GetSpec();
        $uriB = $oPathB->GetSpec();
		
        if (is_link($uriA)) {
            $oApp->AddStatus(": it's a link");
            if (KF_DO_DEL_LINKS) {
                $oApp->AddFolderStatus("- deleting [$uriA]...");
                $ofA = new cFolder($uriA);
                $ok = $ofA->Delete();	// delete the link, not what's inside it (might be elsewhere)
                if ($ok) {
                    $oApp->AddStatus(" done.");
                } else {
                    $oApp->AddStatus(" failed.");
                }
                $oApp->FinishStatus();
            } else {
                $oApp->AddFolderStatus("- skipping");
            }
        } else {
            if (self::InBlacklist($uriA)) {
                // log blacklist hit:
                $oApp->RecordBlacklisted($uriA);
                $oApp->AddFolderStatus(": blacklisted");
                if (KF_DO_DEL_IGNORED) {
                    $oApp->AddFolderStatus(" - deleting");
                    $oApp->FinishStatus();
                    $of = new cFolder($uriA);
                    $of->DeleteTree();
                    $oApp->ShowMessage(" < DELETED [$uriA]");
                }
            } else {
                //$oApp->AddStatus("...");
            
                $arDirA = cFolder::GetDirectoryStringArraysFor($uriA);
                // ['fi'] = array of files
                // ['fo'] = array of folders

                // #### FILES ####
                
                $nFi = 0;
                if (array_key_exists('fi',$arDirA)) {
                    $arFi = $arDirA['fi'];
                    if (is_array($arFi)) {
                        $nFi = count($arFi);
                    }
                }
                
                // for each A file, see if present on B
                if ($nFi > 0) {
                    $dxFi = 0;
                    foreach ($arFi as $fn => $urlA) {
                        $dxFi ++;
                        $oSubA = $oPathA->AddSegment($fn);
                        $oSubB = $oPathB->AddSegment($fn);
                        
                        //$urlB = $uriB.'/'.$fn;
                        //$this->EnsureFile($urlA,$urlB,$nFi,$dxFi);
                        $this->EnsureFile($oSubA,$oSubB,$nFi,$dxFi);
                    }
                } else {
                    //$oApp->ShowMessage("** NO FILES IN [$uriA]");	// debugging
                }

                // #### FOLDERS ####

                if (array_key_exists('fo',$arDirA)) {
                    $arFo = $arDirA['fo'];
                    $nFo = count($arFo);
                } else {
                    $nFo = 0;
                }
            
                if ($nFo > 0) {
                    if (KF_DO_COPY_EMPTY_FOLDERS) {
                    $this->CreateFolderInB();	// ensure that the folder exists on B
                    // This is a bit of a kluge, because if we have folder chain A/B/C where C is empty,
                    // 	and B contains only C and A contains only B, A and B won't get copied,
                    //	and then C can't be created with just a mkdir().
                    // Eventually write a function that can create folder-chains from a spec, maybe.
                    }
                    
                    $dxFo = 0;
                    foreach ($arFo as $fn => $urlA) {
                    $dxFo ++;
                    
                    $oSubA = $oPathA->AddSegment($fn);
                    $oSubB = $oPathB->AddSegment($fn);
                    
                    $this->EnsureFolder($oSubA,$oSubB,$nFo,$dxFo);
                    }
                } else {
                    // nothing inside folder
                    if (KF_DO_COPY_EMPTY_FOLDERS) {
                        // create it on B anyway
                        $this->CreateFolderInB();
                    }
                }
                //$oApp->FinishStatus();
                $this->MaybeDeleteFolder();
            }
        }
    }
    // ACTION: checks for remote protocol, mounts if needed, then passes modified path(s) to EnsureLocalFolder()
    protected function EnsureFolder(cPathSeg $oPathA,cPathSeg $oPathB, $n,$dx) {
        global $oApp;
        
        $oStat = new cPathCompareStatus($oPathA,$n,$dx);
        $oApp->UpdateFolder($oStat);
        $oPair = new cFolderPair($oPathA,$oPathB);
        $oPair->Ensure($oStat);
    }
    protected function EnsureFile(cPathSeg $oPathA,cPathSeg $oPathB,$n,$dx) {
        global $oApp;

        $oStat = new cPathCompareStatus($oPathA,$n,$dx);
        $oApp->UpdateFile($oStat);
        $oPair = new cFilePair($oPathA,$oPathB,$n,$dx);
        $oPair->Ensure($oStat);
    }
    protected function CreateFolderInB() {
        global $oApp;

        $oPathB = $this->GetPathB();
        //$uriB = $oPathB->GetSpec();
        $oFldrB = $oPathB->GetFolderNode();
        if (!$oFldrB->GetExists()) {		// folder not found on B
            $oPathA = $this->GetPathA();
            $oNodeA = $oPathA->GetFolderNode();
            
            $dt = $oNodeA->GetTimestamp();
            $oFldrB->Create();
            $oFldrB->SetTimestamp($dt);

            $oApp->UpdateNewFolder($oPathB);	// display/log action
        }
    }
    protected function MaybeDeleteFolder() {
        global $oApp;
        
        if (KF_DO_DEL) {
            $fs = $this->GetPathA()->GetSpec();
            $oApp->UpdateStatusAlways("DELETING FOLDER [$fs]: ");
            // check to see if folder is empty
    //	    $arFiles = glob("$fs/*");	// glob() seems to be the only dirlisting f() that doesn't include '.' & '..'
            $onDir = new \GlobIterator("$fs/*",\FilesystemIterator::CURRENT_AS_FILEINFO);
            //$nFiles = count($arFiles);
            $nFiles = $onDir->count();
            if ($nFiles == 0) {
                $ok = @rmdir($fs);
                if (!$ok) {
                    $oApp->AddStatus("failed.");		// this isn't fatal; just leave it there
                } else {
                    $oApp->AddStatus("deleted.");
                }
            } else {
                $sFiles = '';
                foreach($onDir as $onFile) {
                    $fn = $onFile->getFilename();
                    $sType = $onFile->getType();
                    $sPerms = sprintf('%o',$onFile->getPerms());
                    $sFiles .= "\n * ($sType, $sPerms) $fn";
                }

                $sStatus = "still has $nFiles file(s):$sFiles";
                $oApp->AddStatus($sStatus);
                $oApp->RecordDeletion("$fs\n - $sStatus\n");
            }
            $oApp->FinishStatus();
        }
    }
    static protected function InBlacklist($fp) {
        global $arFilenameBlacklist;
        
        foreach ($arFilenameBlacklist as $sRegex) {
            $ok = preg_match($sRegex,$fp);
            if ($ok === 1) {
                return TRUE;
            } elseif ($ok === FALSE) {
                die("Inexplicable regex failure: pattern=[$sRegex] subject=[$fp]\n");
            }
            return FALSE;
        }
    }
}
