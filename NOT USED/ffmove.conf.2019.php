<?php
/*
  TODO:
  * option to *not* compare files already found (only compare after copying)
  * significantly improved display of progress (folders should be in tree so they don't wrap around)
*/

// ACTION FLAGS

define('KF_DOING_OFFLOAD',FALSE);

define('KF_DO_COPY',		TRUE);	// TRUE = if file A missing from B, copy it
define('KF_DO_DEL',		KF_DOING_OFFLOAD);	// TRUE = after verifying that B=A, delete A
define('KF_DO_DEL_LINKS',	KF_DOING_OFFLOAD);	// TRUE = if A is a link, delete it
define('KF_DO_DEL_IGNORED',	KF_DOING_OFFLOAD);	// TRUE = when blacklisted folder found, delete it and everything under it
define('KF_DO_COMP_FOUND',	FALSE);
define('KF_DO_COPY_EMPTY_FOLDERS',	TRUE);

// DISPLAY FLAGS

  // display all folders as they are processed:
define('KBIN_SHOW_FOLDERS',	KF_STATUS_SHOW);		
  // display all files as they are processed
define('KBIN_SHOW_FILES',	KF_STATUS_FILE | KF_STATUS_PATH);
  // display copying process
define('KBIN_SHOW_NEW_FILES',	KF_STATUS_FILE | KF_STATUS_PATH | KF_STATUS_SHOW);
  // display folders created
define('KBIN_SHOW_NEW_FOLDERS',	KF_STATUS_FILE | KF_STATUS_PATH | KF_STATUS_SHOW | KF_STATUS_NEWLINE);
// only show read process for files larger than this
define('KN_SHOW_READ_MIN_BYTES',	1024*1024);

// OTHER OPTION

// list of masks for files that Really Just Don't Matter, so they can be ignored/deleted
$arFilenameBlacklist = array(	// TODO: rename to arFileNamesIgnore
      '/cache/i'	// skip cache folders (anything whose name includes "cache", case-insensitive)
      //'/^cache$/i'	// skip cache folders (anything named "cache", case-insensitive)
      );


// STATLER-NEXTCLOUD on SamEagle backup
// HARENA
//define('KFS_URL_A','sftp:root@sameagle.local//media/woozle/TeraLap1/home/woozle/nextcloud-statler/Harena');
//define('KFS_URL_B','/mnt/storage/www-data/nextcloud/admin/files/commons/Harena');

// WOOZLE
define('KFS_URL_A','sftp:root@sameagle.local//media/woozle/TeraLap1/home/woozle/nextcloud-statler/woozle');
define('KFS_URL_B','/mnt/storage/www-data/nextcloud/admin/files/commons/woozle');
