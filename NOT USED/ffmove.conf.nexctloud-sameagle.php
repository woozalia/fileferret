<?php

define('KF_DOING_OFFLOAD',FALSE);

define('KF_DO_COPY',		TRUE);	// TRUE = if file A missing from B, copy it
define('KF_DO_DEL',		KF_DOING_OFFLOAD);	// TRUE = after verifying that B=A, delete A
define('KF_DO_DEL_LINKS',	KF_DOING_OFFLOAD);	// TRUE = if A is a link, delete it
define('KF_DO_DEL_IGNORED',	KF_DOING_OFFLOAD);	// TRUE = when blacklisted folder found, delete it and everything under it

// list of masks for files that Really Just Don't Matter, so they can be ignored/deleted
$arFilenameBlacklist = array(	// TODO: rename to arFileNamesIgnore
      '/cache/i'	// skip cache folders (anything whose name includes "cache", case-insensitive)
      //'/^cache$/i'	// skip cache folders (anything named "cache", case-insensitive)
      );


// STATLER-NEXTCLOUD on SamEagle backup
define('KFS_URL_A','sftp:root@sameagle.local//media/woozle/TeraLap1/home/woozle/nextcloud-statler');
define('KFS_URL_B','/mnt/HexTera2/archive/collections/nextcloud-statler');
